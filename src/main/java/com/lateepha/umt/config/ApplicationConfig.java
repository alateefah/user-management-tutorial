package com.lateepha.umt.config;

import com.lateepha.umt.service.PermissionService;
import com.lateepha.umt.service.RolePermissionService;
import com.lateepha.umt.service.RoleService;
import com.lateepha.umt.util.exception.GeneralAppExceptionMapper;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.core.Application;


@javax.ws.rs.ApplicationPath("/api")

public class ApplicationConfig extends Application{
    @Override    
    public Set<Class<?>> getClasses() {
        Set<Class<?>> s = new HashSet<Class<?>>();
        
        // add services here
		
        s.add(GeneralAppExceptionMapper.class);
        
        s.add(PermissionService.class);
        
        s.add(RoleService.class);
        
        s.add(RolePermissionService.class);
        
        return s;
    }
}
