package com.lateepha.umt.data.manager;

import com.lateepha.umt.pojo.AppBoolean;
import javax.ejb.Stateless;

@Stateless
public class BooleanManager implements BooleanManagerLocal {
    
    
    @Override
    public AppBoolean returnBoolean (Boolean state) {
        AppBoolean appBoolean = new AppBoolean();
        
        appBoolean.setState(state);
        
        return appBoolean;
    }
}

