package com.lateepha.umt.data.manager;

import com.lateepha.umt.pojo.AppBoolean;

public interface BooleanManagerLocal {
   
    AppBoolean returnBoolean(Boolean State);        
    
}
