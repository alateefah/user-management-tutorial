package com.lateepha.umt.data.manager;

import javax.ejb.Stateless;
import javax.ws.rs.core.Response;
import com.lateepha.umt.util.exception.GeneralAppException;


@Stateless
public class ExceptionThrowerManager implements ExceptionThrowerManagerLocal {
    
       
    private final String INCOMPLETE_DATA_ERROR = "Incomplete data";
    private final String INCOMPLETE_DATA_ERROR_DETAILS = "Incomplete has been provided";

    private final String INVALID_TOKEN_ERROR = "Invalid token";
    private final String INVALID_TOKEN_ERROR_DETAILS = "Invalid or expired token supplied";
    
    private final String INVALID_INTEGER_ERROR = "Invalid integer";
    private final String INVALID_INTEGER_ERROR_DETAILS = "Invalid integer supplied";
    
    private final String PERMISSION_ALREADY_EXIST_ERROR = "Permission already exist";
    private final String PERMISSION_ALREADY_EXIST_ERROR_DETAILS = "A permission with this id already exists";
    
    private final String PERMISSION_DOES_NOT_EXIST_ERROR = "Permission does not exist";
    private final String PERMISSION_DOES_NOT_EXIST_ERROR_DETAILS = "No permission with this id in the database";
    
    private final String ROLE_ALREADY_EXIST_ERROR = "Role already exist";
    private final String ROLE_ALREADY_EXIST_ERROR_DETAILS = "A role with this id already exists";
    
    private final String ROLE_DOES_NOT_EXIST_ERROR = "Role does not exist";
    private final String ROLE_DOES_NOT_EXIST_ERROR_DETAILS = "No role with this id in the database";
    
    private final String PERMISSION_ALREADY_ASSIGNED_TO_ROLE_ERROR = "Role Permission already exist";
    private final String PERMISSION_ALREADY_ASSIGNED_TO_ROLE_ERROR_DETAILS = "This permission is already assigned to this role";
    
    
    @Override
    public void throwNullUserAttributesException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                    400, INCOMPLETE_DATA_ERROR, INCOMPLETE_DATA_ERROR_DETAILS, link);
    }

    @Override
    public void throwInvalidTokenException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                    400, INVALID_TOKEN_ERROR, INVALID_TOKEN_ERROR_DETAILS, link);
    }
    
    @Override
    public void throwInvalidIntegerAttributeException(String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                    400, INVALID_INTEGER_ERROR, INVALID_INTEGER_ERROR_DETAILS, link);
    }  
    
    @Override
    public void throwPermissionAlreadyExistException (String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                    400, PERMISSION_ALREADY_EXIST_ERROR, PERMISSION_ALREADY_EXIST_ERROR_DETAILS, link);
    }
 
    @Override
    public void throwPermissionDoesNotExistException (String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                    400, PERMISSION_DOES_NOT_EXIST_ERROR, PERMISSION_DOES_NOT_EXIST_ERROR_DETAILS, link);
    }
    
    @Override
    public void throwRoleAlreadyExistException (String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                    400, ROLE_ALREADY_EXIST_ERROR, ROLE_ALREADY_EXIST_ERROR_DETAILS, link);
    }
    
        @Override
    public void throwRoleDoesNotExistException (String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                    400, ROLE_DOES_NOT_EXIST_ERROR, ROLE_DOES_NOT_EXIST_ERROR_DETAILS, link);
    }
    
     @Override
    public void throwPermissionAlreadyAssignedToRoleException (String link) throws GeneralAppException {
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                    400, PERMISSION_ALREADY_ASSIGNED_TO_ROLE_ERROR, 
                            PERMISSION_ALREADY_ASSIGNED_TO_ROLE_ERROR_DETAILS, link);
    }
}
