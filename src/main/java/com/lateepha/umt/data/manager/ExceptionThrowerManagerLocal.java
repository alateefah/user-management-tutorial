package com.lateepha.umt.data.manager;

import com.lateepha.umt.util.exception.GeneralAppException;

public interface ExceptionThrowerManagerLocal {
   
    void throwNullUserAttributesException(String link) throws GeneralAppException;
    
    void throwInvalidTokenException(String link) throws GeneralAppException;
    
    void throwInvalidIntegerAttributeException(String link) throws GeneralAppException;
    
    void throwPermissionAlreadyExistException(String link) throws GeneralAppException;
    
    void throwPermissionDoesNotExistException (String link) throws GeneralAppException;
    
    void throwRoleAlreadyExistException (String link) throws GeneralAppException;
    
    void throwRoleDoesNotExistException (String link) throws GeneralAppException;
        
    void throwPermissionAlreadyAssignedToRoleException (String link) throws GeneralAppException;
    
}
