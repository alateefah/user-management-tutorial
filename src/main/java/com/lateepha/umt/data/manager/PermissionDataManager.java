package com.lateepha.umt.data.manager;

import com.lateepha.umt.data.provider.DataProviderLocal;
import com.lateepha.umt.model.Permission;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

@Stateless
public class PermissionDataManager implements  PermissionDataManagerLocal {
    
    @EJB
    private DataProviderLocal crud;    

    @Override
    public Permission create(Permission permission) throws EJBTransactionRolledbackException {
        return crud.create(permission);
    }

    @Override
    public Permission update(Permission permission) {
        return crud.update(permission);
    }

    @Override
    public Permission get(String permissionId) {
        return crud.find(permissionId, Permission.class);
    }

    @Override
    public void delete(Permission permission) {
        crud.delete(permission);
    }
  
    @Override
    public List<Permission> getAll() {    
        return crud.findAll(Permission.class);
    }
}
