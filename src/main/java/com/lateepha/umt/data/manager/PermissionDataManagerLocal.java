package com.lateepha.umt.data.manager;

import com.lateepha.umt.model.Permission;
import java.util.List;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Local;

@Local
public interface PermissionDataManagerLocal {
    
    Permission create(Permission permission) throws EJBTransactionRolledbackException;
    
    Permission update(Permission permission);
    
    Permission get(String permissionId);
    
    void delete (Permission permission);
    
    List<Permission> getAll();
}
