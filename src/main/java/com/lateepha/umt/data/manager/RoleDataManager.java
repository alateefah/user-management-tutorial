package com.lateepha.umt.data.manager;

import com.lateepha.umt.data.provider.DataProviderLocal;
import com.lateepha.umt.model.Role;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

@Stateless
public class RoleDataManager implements  RoleDataManagerLocal {
    
    @EJB
    private DataProviderLocal crud;    

    @Override
    public Role create(Role role) throws EJBTransactionRolledbackException {
        return crud.create(role);
    }

    @Override
    public Role update(Role role) {
        return crud.update(role);
    }

    @Override
    public Role get(String roleId) {
        return crud.find(roleId, Role.class);
    }

    @Override
    public void delete(Role role) {
        crud.delete(role);
    }
  
    @Override
    public List<Role> getAll() {    
        return crud.findAll(Role.class);
    }
}
