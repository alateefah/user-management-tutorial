package com.lateepha.umt.data.manager;

import com.lateepha.umt.model.Role;
import java.util.List;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Local;

@Local
public interface RoleDataManagerLocal {
    
    Role create(Role role) throws EJBTransactionRolledbackException;
    
    Role update(Role role);
    
    Role get(String roleId);
    
    void delete (Role role);
    
    List getAll();
}