package com.lateepha.umt.data.manager;

import com.lateepha.umt.data.provider.DataProviderLocal;
import com.lateepha.umt.model.RolePermission;
import java.util.HashMap;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

@Stateless
public class RolePermissionDataManager implements  RolePermissionDataManagerLocal {
    
    @EJB
    private DataProviderLocal crud;    

    @Override
    public RolePermission create(RolePermission rolePermission) throws EJBTransactionRolledbackException {
        return crud.create(rolePermission);
    }

    @Override
    public List<RolePermission> get(RolePermission rolePermission) {
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("roleId", rolePermission.getRoleId());
        parameters.put("permissionId", rolePermission.getPermissionId());
        return crud.findByNamedQuery("RolePermission.findRolePermission",
                parameters, RolePermission.class);
    }
    
    @Override
    public List<RolePermission> getRolePermissions(String roleId) {
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("roleId", roleId);
        return crud.findByNamedQuery("RolePermission.getRolePermissions",
                parameters, RolePermission.class);   
    }
    
    @Override
    public void delete(RolePermission rolePermission) {
        crud.delete(rolePermission);
    }
    
}
