package com.lateepha.umt.data.manager;

import com.lateepha.umt.model.RolePermission;
import java.util.List;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Local;

@Local
public interface RolePermissionDataManagerLocal {
    
    RolePermission create(RolePermission rolePermission) throws EJBTransactionRolledbackException;
    
    List<RolePermission> get(RolePermission rolePermission);
    
    List<RolePermission> getRolePermissions(String roleId);
    
    void delete (RolePermission rolePermission);
    
}
