package com.lateepha.umt.manager;

import com.lateepha.umt.data.manager.BooleanManagerLocal;
import com.lateepha.umt.data.manager.ExceptionThrowerManagerLocal;
import com.lateepha.umt.data.manager.PermissionDataManagerLocal;
import com.lateepha.umt.model.Permission;
import com.lateepha.umt.pojo.AppBoolean;
import com.lateepha.umt.pojo.AppPermission;
import com.lateepha.umt.util.Verifier;
import com.lateepha.umt.util.exception.GeneralAppException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;


@Stateless
public class PermissionManager implements PermissionManagerLocal {
    
    @EJB
    Verifier verifier;  //a java class that checks if paramater is null
    
    @EJB
    PermissionDataManagerLocal permissionDataManager; 
    
    @EJB
    ExceptionThrowerManagerLocal exceptionThrowerManager;
    
    @EJB
    BooleanManagerLocal booleanManager;
    
    private final String PERMISSION_LINK = "/permission";
    
    @Override
    public AppPermission createPermission(String permissionId, String description) throws GeneralAppException {
        //check value of permissionId and description and throw an error is any os null
        verifier.setResourceUrl(PERMISSION_LINK).verifyParams(permissionId, description);
        
        
        //check if a permission with the id entered already exists and throw an error if true
        if (permissionDataManager.get(permissionId) != null) {
            exceptionThrowerManager.throwPermissionAlreadyExistException(PERMISSION_LINK);
        }
        
        Permission permission = new Permission();
        permission.setPermissionId(permissionId);
        permission.setDescription(description);
        
        permissionDataManager.create(permission);
        
        return getAppPermission(permission);
    }
    
    @Override
    public List<AppPermission> getAllPermissions () throws GeneralAppException {
        List<Permission> permissions = permissionDataManager.getAll();
        
        List<AppPermission> appPermissions = new ArrayList<AppPermission>();
        for (Permission permission: permissions) {
            //convert model to pojo and add to pojo list
            appPermissions.add(getAppPermission(permission));
        }
        
        return appPermissions;
    }
    
    @Override
    public AppPermission updatePermission(String permissionId, String description) throws GeneralAppException {
        //check value of permissionId and new description and throw an error is any os null. we are inly updating the description of course
        verifier.setResourceUrl(PERMISSION_LINK).verifyParams(permissionId, description);
        
        Permission permission = permissionDataManager.get(permissionId);
        //check if the permission id we want to update exists and throw an error if true
        if (permission == null) {
            exceptionThrowerManager.throwPermissionDoesNotExistException(PERMISSION_LINK);
        }
        
        permission.setDescription(description);
        permissionDataManager.update(permission);
        
        return getAppPermission(permission);
    }
    
    @Override
    public AppBoolean deletePermission (String permissionId) throws GeneralAppException {
        verifier.setResourceUrl(PERMISSION_LINK).verifyParams(permissionId);
          
        //get permission with the id entered
        Permission permission = permissionDataManager.get(permissionId);
        
        //check if the permission id we want to update exists and throw an error if true
        if (permission == null) {
            exceptionThrowerManager.throwPermissionDoesNotExistException(PERMISSION_LINK);
        }
        
        //delete it
        permissionDataManager.delete(permission);
        
        return booleanManager.returnBoolean(true);
    }
    
    // method to return data from model
    @Override
    public AppPermission getAppPermission (Permission permission) {
        AppPermission appPermission = new AppPermission();
        
        appPermission.setPermissionId(permission.getPermissionId());
        appPermission.setDescription(permission.getDescription());
        
        return appPermission;
    }
}