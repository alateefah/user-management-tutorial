package com.lateepha.umt.manager;

import com.lateepha.umt.model.Permission;
import com.lateepha.umt.pojo.AppBoolean;
import com.lateepha.umt.pojo.AppPermission;
import com.lateepha.umt.util.exception.GeneralAppException;
import java.util.List;


public interface PermissionManagerLocal {
    
    AppPermission createPermission(String permissionId, String description) throws GeneralAppException;
    
    List<AppPermission> getAllPermissions () throws GeneralAppException;
    
    AppPermission updatePermission(String permissionId, String description) throws GeneralAppException;
    
    AppBoolean deletePermission (String permissionId) throws GeneralAppException;
    
    AppPermission getAppPermission (Permission permission);
}
