package com.lateepha.umt.manager;

import com.lateepha.umt.data.manager.BooleanManagerLocal;
import com.lateepha.umt.data.manager.ExceptionThrowerManagerLocal;
import com.lateepha.umt.data.manager.RoleDataManagerLocal;
import com.lateepha.umt.model.Role;
import com.lateepha.umt.pojo.AppBoolean;
import com.lateepha.umt.pojo.AppRole;
import com.lateepha.umt.util.Verifier;
import com.lateepha.umt.util.exception.GeneralAppException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;


@Stateless
public class RoleManager implements RoleManagerLocal {
    
    @EJB
    Verifier verifier;  //a java class that checks if paramater is null
    
    @EJB
    RoleDataManagerLocal roleDataManager; 
    
    @EJB
    ExceptionThrowerManagerLocal exceptionThrowerManager;
    
    @EJB
    BooleanManagerLocal booleanManager;
    
    private final String PERMISSION_LINK = "/role";
    
    @Override
    public AppRole createRole(String roleId, String description) throws GeneralAppException {
        //check value of roleId and description and throw an error is any os null
        verifier.setResourceUrl(PERMISSION_LINK).verifyParams(roleId, description);
        
        
        //check if a role with the id entered already exists and throw an error if true
        if (roleDataManager.get(roleId) != null) {
            exceptionThrowerManager.throwRoleAlreadyExistException(PERMISSION_LINK);
        }
        
        Role role = new Role();
        role.setRoleId(roleId);
        role.setDescription(description);
        
        roleDataManager.create(role);
        
        return getAppRole(role);
    }
    
    @Override
    public List<AppRole> getAllRoles () throws GeneralAppException {
        List<Role> roles = roleDataManager.getAll();
        
        List<AppRole> appPermissions = new ArrayList<AppRole>();
        for (Role role: roles) {
            appPermissions.add(getAppRole(role));
        }
        
        return appPermissions;
    }
    
    @Override
    public AppRole updateRole(String roleId, String description) throws GeneralAppException {
        verifier.setResourceUrl(PERMISSION_LINK).verifyParams(roleId, description);
        
        Role role = roleDataManager.get(roleId);
        if (role == null) {
            exceptionThrowerManager.throwRoleDoesNotExistException(PERMISSION_LINK);
        }
        
        role.setDescription(description);
        roleDataManager.update(role);
        
        return getAppRole(role);
    }
    
    @Override
    public AppBoolean deleteRole (String roleId) throws GeneralAppException {
        verifier.setResourceUrl(PERMISSION_LINK).verifyParams(roleId);
          
        Role role = roleDataManager.get(roleId);
        
        if (role == null) {
            exceptionThrowerManager.throwRoleDoesNotExistException(PERMISSION_LINK);
        }
        
        roleDataManager.delete(role);
        
        return booleanManager.returnBoolean(true);
    }
    
    // method to return data from model
    @Override
    public AppRole getAppRole (Role role) {
        AppRole appRole = new AppRole();
        
        appRole.setRoleId(role.getRoleId());
        appRole.setDescription(role.getDescription());
        
        return appRole;
    }
    
    
}