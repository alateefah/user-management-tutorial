package com.lateepha.umt.manager;

import com.lateepha.umt.model.Role;
import com.lateepha.umt.pojo.AppBoolean;
import com.lateepha.umt.pojo.AppRole;
import com.lateepha.umt.util.exception.GeneralAppException;
import java.util.List;


public interface RoleManagerLocal {
    
    AppRole createRole(String roleId, String description) throws GeneralAppException;    
    
    List<AppRole> getAllRoles () throws GeneralAppException;
    
    AppRole updateRole(String roleId, String description) throws GeneralAppException;
    
    AppBoolean deleteRole (String rolessId) throws GeneralAppException;
    
    AppRole getAppRole (Role role);
}
