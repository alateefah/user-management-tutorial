package com.lateepha.umt.manager;

import com.lateepha.umt.data.manager.BooleanManagerLocal;
import com.lateepha.umt.data.manager.ExceptionThrowerManagerLocal;
import com.lateepha.umt.data.manager.PermissionDataManagerLocal;
import com.lateepha.umt.data.manager.RoleDataManagerLocal;
import com.lateepha.umt.data.manager.RolePermissionDataManagerLocal;
import com.lateepha.umt.model.Permission;
import com.lateepha.umt.model.Role;
import com.lateepha.umt.model.RolePermission;
import com.lateepha.umt.pojo.AppPermission;
import com.lateepha.umt.pojo.AppRolePermission;
import com.lateepha.umt.pojo.RolePermissionsPayload;
import com.lateepha.umt.util.Verifier;
import com.lateepha.umt.util.exception.GeneralAppException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;


@Stateless
public class RolePermissionManager implements RolePermissionManagerLocal {
    
    @EJB
    Verifier verifier;  
    
    @EJB
    PermissionManagerLocal permissionManager;
    
    @EJB
    RoleManagerLocal roleManager;
    
    @EJB
    RolePermissionDataManagerLocal rolePermissionDataManager; 
    
    @EJB
    PermissionDataManagerLocal permissionDataManager; 
    
    @EJB
    RoleDataManagerLocal roleDataManager; 
    
    @EJB
    ExceptionThrowerManagerLocal exceptionThrowerManager;
    
    @EJB
    BooleanManagerLocal booleanManager;
    
    private final String PERMISSION_LINK = "/role-permission";
    
    @Override
    public AppRolePermission assignPermissionToRole(String permissionId, String roleId) throws GeneralAppException {
        
        verifier.setResourceUrl(PERMISSION_LINK).verifyParams(permissionId, roleId);
        
        RolePermission rolePermission = new RolePermission();
        rolePermission.setPermissionId(permissionId);
        rolePermission.setRoleId(roleId);
        
        //check if role permission already exists
        if (!rolePermissionDataManager.get(rolePermission).isEmpty()) {
            exceptionThrowerManager.throwPermissionAlreadyAssignedToRoleException(PERMISSION_LINK);
        }
        
        rolePermissionDataManager.create(rolePermission);
        
        return getAppRolePermission(rolePermission);
    }
    
    
    @Override
    public RolePermissionsPayload assignPermissionsToRole(String permissionIds, String roleId) throws GeneralAppException {        
        verifier.setResourceUrl(PERMISSION_LINK).verifyParams(permissionIds, roleId);
        
        /*
        * Let's make the role exist
        */
        Role role = roleDataManager.get(roleId);        
        if (role == null) {
            exceptionThrowerManager.throwRoleDoesNotExistException(PERMISSION_LINK);
        }
        
        /*
        * Let's rmove all associated permissions to the role. To do this, we
        * 1. get role permissions
        * 2. delete one after the other
        */
        List<RolePermission> rolePermissions = rolePermissionDataManager.getRolePermissions(roleId); // deleteRolePermissions(roleId);
        for (int i = 0; i < rolePermissions.size(); i++) {            
            rolePermissionDataManager.delete(rolePermissions.get(i));
        }
        
        
        RolePermissionsPayload rolePermissionsPayload = new RolePermissionsPayload();        
        List<AppPermission> appPermissions = new ArrayList<AppPermission>();
        
        String[] listOfPermissions = permissionIds.split("\\s*,\\s*");
        for (int i = 0; i < listOfPermissions.length; i++) {
            
            Permission permission = permissionDataManager.get(listOfPermissions[i]);
            if (permission == null) {
                exceptionThrowerManager.throwPermissionDoesNotExistException(PERMISSION_LINK);
            }
            RolePermission rolePermission = new RolePermission();
            
            rolePermission.setPermissionId(listOfPermissions[i]);
            rolePermission.setRoleId(roleId);
            
            if (!rolePermissionDataManager.get(rolePermission).isEmpty()) {
                rolePermissionDataManager.create(rolePermission);
            }
            
            appPermissions.add(permissionManager.getAppPermission(permission));
        }
        rolePermissionsPayload.setPermissions(appPermissions);
        rolePermissionsPayload.setRoleId(roleManager.getAppRole(role));
        
        return rolePermissionsPayload;
    }
    
    public AppRolePermission getAppRolePermission (RolePermission rolePermission) {
        AppRolePermission appRolePermission = new AppRolePermission();
        
        appRolePermission.setPermissionId(rolePermission.getPermissionId());
        appRolePermission.setRoleId(rolePermission.getRoleId());
        
        return appRolePermission;
    }
}