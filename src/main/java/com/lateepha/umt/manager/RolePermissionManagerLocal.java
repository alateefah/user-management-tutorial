package com.lateepha.umt.manager;

import com.lateepha.umt.pojo.AppRolePermission;
import com.lateepha.umt.pojo.RolePermissionsPayload;
import com.lateepha.umt.util.exception.GeneralAppException;

public interface RolePermissionManagerLocal {
    
    AppRolePermission assignPermissionToRole(String permissionId, String roleId) throws GeneralAppException;
    
    RolePermissionsPayload assignPermissionsToRole(String permissionIds, String roleId) throws GeneralAppException;
    
}
