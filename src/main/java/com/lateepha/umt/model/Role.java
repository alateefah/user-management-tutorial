package com.lateepha.umt.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Lateefah
 */

@Entity
@Table(name = "role")

public class Role {
    
    @Id
    @NotNull
    @Column(name = "role_id")
    private String roleId;    
        
    @NotNull
    @Column(name = "description")
    private String description;

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }    
    
}
