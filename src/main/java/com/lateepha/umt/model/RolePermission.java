package com.lateepha.umt.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "role_permission")

/*
** The @@NamedQueries allows us state custom sql statement to be executed on our database table
** in RolePermission.findRolePermission, we are using it to fetch a single record of role_permission with 
** the supplied credential.
*/
@NamedQueries({
    @NamedQuery(name = "RolePermission.findRolePermission", query = "SELECT rp FROM RolePermission rp WHERE rp.roleId = :roleId AND rp.permissionId = :permissionId"),
    @NamedQuery(name = "RolePermission.getRolePermissions", query = "SELECT rp FROM RolePermission rp WHERE rp.roleId = :roleId")
})

public class RolePermission {
    
    @Id
    @NotNull
    @Column(name = "permission_id")
    private String permissionId;    
      
    @Id
    @NotNull
    @Column(name = "role_id")
    private String roleId;

    public String getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(String permissionId) {
        this.permissionId = permissionId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }   
}
