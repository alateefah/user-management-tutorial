package com.lateepha.umt.pojo;

import java.io.Serializable;

public class AppBoolean implements Serializable {
    
    Boolean state;
    
    public AppBoolean() {}
    
    public Boolean getState() {
        return state;
    }
    
    public void setState(Boolean state) {
        this.state = state;
    }

   
}
