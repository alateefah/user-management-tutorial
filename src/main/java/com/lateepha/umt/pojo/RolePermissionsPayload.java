package com.lateepha.umt.pojo;

import java.util.List;


public class RolePermissionsPayload {
    
    private AppRole roleId;
    private List<AppPermission> permissions;

    public AppRole getRoleId() {
        return roleId;
    }

    public void setRoleId(AppRole roleId) {
        this.roleId = roleId;
    }

    public List<AppPermission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<AppPermission> permissions) {
        this.permissions = permissions;
    }
    
}
