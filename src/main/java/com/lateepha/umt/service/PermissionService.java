package com.lateepha.umt.service;

import com.lateepha.umt.manager.PermissionManagerLocal;
import com.lateepha.umt.pojo.AppBoolean;
import com.lateepha.umt.pojo.AppPermission;
import com.lateepha.umt.util.exception.GeneralAppException;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Stateless
@Path("/v1/permission")
public class PermissionService {
    
    @Context
    HttpServletRequest request;   
    
    @EJB    
    PermissionManagerLocal permissionManager;
       
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response createPermission(@QueryParam("permission-id") String permissionId,
                                      @QueryParam("description") String description) throws GeneralAppException {  
        
        AppPermission appPermission = permissionManager.createPermission(permissionId, description);
        return Response.ok(appPermission).build();
           
    }
    
    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllPermissions() throws GeneralAppException {  
        
        List<AppPermission> appPermissionList = permissionManager.getAllPermissions();
        return Response.ok(new GenericEntity<List<AppPermission>>(appPermissionList) {}).build();
           
    }
    
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response updatePermission (@QueryParam("permission-id") String permissionId,
                                      @QueryParam("description") String description) throws GeneralAppException {  
        
        AppPermission appPermission = permissionManager.updatePermission(permissionId, description);
        return Response.ok(appPermission).build();
           
    }
    
    
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deletePermission(@QueryParam("permission-id") String permissionId) throws GeneralAppException {  
        
        AppBoolean appBoolean = permissionManager.deletePermission(permissionId);
        return Response.ok(appBoolean).build();
           
    }
    
    
}