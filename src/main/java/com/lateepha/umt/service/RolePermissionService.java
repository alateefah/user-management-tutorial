package com.lateepha.umt.service;

import com.lateepha.umt.manager.RolePermissionManagerLocal;
import com.lateepha.umt.pojo.AppRolePermission;
import com.lateepha.umt.pojo.RolePermissionsPayload;
import com.lateepha.umt.util.exception.GeneralAppException;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Stateless
@Path("/v1/role-permission")
public class RolePermissionService {
    
    @Context
    HttpServletRequest request;   
    
    @EJB    
    RolePermissionManagerLocal rolePermissionManager;
       
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response assignPermissionToRole(@QueryParam("permission-id") String permissionId,
                                      @QueryParam("role-id") String roleId) throws GeneralAppException {  
        
        AppRolePermission appRolePermission = rolePermissionManager.assignPermissionToRole(permissionId, roleId);
        return Response.ok(appRolePermission).build();
           
    }
    
    @Path("/multiple")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response assignPermissionsToRole(@QueryParam("permission-ids") String permissionIds,
                                      @QueryParam("role-id") String roleId) throws GeneralAppException {  
        
        RolePermissionsPayload rolePermissionPayload = rolePermissionManager.assignPermissionsToRole(permissionIds, roleId);
        return Response.ok(rolePermissionPayload).build();
           
    }
    
    
}