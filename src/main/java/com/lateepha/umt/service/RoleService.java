package com.lateepha.umt.service;

import com.lateepha.umt.manager.RoleManagerLocal;
import com.lateepha.umt.pojo.AppBoolean;
import com.lateepha.umt.pojo.AppRole;
import com.lateepha.umt.util.exception.GeneralAppException;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Stateless
@Path("/v1/role")
public class RoleService {
    
    @Context
    HttpServletRequest request;   
    
    @EJB    
    RoleManagerLocal roleManager;
       
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response createRole(@QueryParam("role-id") String roleId,
                                      @QueryParam("description") String description) throws GeneralAppException {  
        
        AppRole appRole = roleManager.createRole(roleId, description);
        return Response.ok(appRole).build();
           
    }
        
    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllRoles() throws GeneralAppException {  
        
        List<AppRole> appRoleList = roleManager.getAllRoles();
        return Response.ok(new GenericEntity<List<AppRole>>(appRoleList) {}).build();
           
    }
    
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateRole (@QueryParam("role-id") String roleId,
                                      @QueryParam("description") String description) throws GeneralAppException {  
        
        AppRole appRole = roleManager.updateRole(roleId, description);
        return Response.ok(appRole).build();
           
    }
    
    
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteRole(@QueryParam("role-id") String roleId) throws GeneralAppException {  
        
        AppBoolean appBoolean = roleManager.deleteRole(roleId);
        return Response.ok(appBoolean).build();
           
    }
    
}