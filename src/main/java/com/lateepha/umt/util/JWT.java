package com.lateepha.umt.util;

import javax.xml.bind.DatatypeConverter;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import javax.ejb.Stateless;
import com.lateepha.umt.util.exception.GeneralAppException;
import javax.ws.rs.core.Response;

@Stateless
public class JWT {    
    SecretKey secretKey = new SecretKey();            

    public Claims parseJWT(String jwt) {
 
        Claims claims = Jwts.parser()         
           .setSigningKey(DatatypeConverter.parseBase64Binary(secretKey.getSecret()))
           .parseClaimsJws(jwt).getBody();
        
        return claims;
    }
       
    public Claims verifyJwt(String rawToken, String resource) throws GeneralAppException {
        try {
            String authToken = rawToken.substring(7);
            return parseJWT(authToken);
        }  catch (Exception e) {
            throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, "Invalid token supplied", "Token supplied could not be parsed",
                resource);
        }
    }
    
}
