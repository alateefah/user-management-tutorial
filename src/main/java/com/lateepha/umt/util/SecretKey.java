package com.lateepha.umt.util;

import javax.ejb.Stateless;

@Stateless
public class SecretKey {
    private String secret = "umt";
    
    public String getSecret(){
        return secret;
    }
}
