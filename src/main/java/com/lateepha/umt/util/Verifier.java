package com.lateepha.umt.util;

import com.lateepha.umt.data.manager.ExceptionThrowerManagerLocal;
import com.lateepha.umt.util.exception.GeneralAppException;
import io.jsonwebtoken.Claims;
import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class Verifier {

    @EJB
    private ExceptionThrowerManagerLocal exceptionManager;
       
    private String resourceUrl;   
    
    public Verifier setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl;
        return this;
    }
    
    public void verifyParams(String... params) throws GeneralAppException {
        for (String param : params) {
            if (param == null || param.isEmpty()) {
                exceptionManager.throwNullUserAttributesException(resourceUrl);
            }
        }
    }        
    
    public Claims verifyJwt(String rawToken) 
            throws GeneralAppException {
        try {
            String authToken = rawToken.substring(7);
            JWT  token = new JWT();  
            return token.parseJWT(authToken);
        }  catch (Exception e) {
            exceptionManager.throwInvalidTokenException(resourceUrl);
        }
        return null;
    }
    
    public void verifyInteger(String... params) throws GeneralAppException {
        for (String param : params) {
            try {
                Integer.parseInt(param);
            } catch (Exception e) {
                exceptionManager.throwInvalidIntegerAttributeException(resourceUrl);
            }            
        }
    } 
        
    
}
