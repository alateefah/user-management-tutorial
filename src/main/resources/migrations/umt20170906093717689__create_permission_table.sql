/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Lateefah Abdulkareem
 * Created: Sep 6, 2017
 */

create table permission (
    permission_id varchar(20) not null,
    description varchar(50) not null,
    primary key (permission_id)
)engine = innodb default charset = utf8;