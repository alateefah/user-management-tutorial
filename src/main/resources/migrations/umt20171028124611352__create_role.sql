/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Lateefah
 * Created: Oct 28, 2017
 */

create table role (
    role_id varchar(15) not null,
    description varchar(50) not null,
    primary key (role_id)
)engine = innodb default charset = utf8;