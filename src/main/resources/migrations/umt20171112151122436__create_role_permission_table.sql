/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Lateefah
 * Created: Nov 12, 2017
 */

create table role_permission (
    role_id varchar(15) not null,
    permission_id varchar(20) not null,
    primary key (role_id, permission_id),
    constraint fk_role_permission_roleId foreign key (role_id) references role(role_id),
    constraint fk_role_permission_permissionId foreign key (permission_id) references permission(permission_id)
) engine = innodb default charset = utf8;